#pragma once

#include "Piece.h"

class Knight : public Piece
{
public:
	Knight(bool isWhite, Point point);
	bool isValidMove(Point point, bool test);
	char returnCharacter();
};