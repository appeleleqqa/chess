#include "Point.h"
//-------------------------------------------------------constructor-------------------------------------------------------------
Point::Point()
{
	_x = 0;
	_y = 0;
}


Point::Point(int x, int y) : _x(x), _y(y)
{}

Point::Point(const Point& other)
{
	_x = other._x;
	_y = other._y;
}
//----------------------------------------------------------getters--------------------------------------------------------------
int Point::getX()
{
	return _x;
}

int Point::getY()
{
	return _y;
}

//----------------------------------------------------------operators------------------------------------------------------------
Point& Point::operator=(const Point& other)
{
	_x = other._x;
	_y = other._y;
	return *this;
}

//-----------------------------------------------------------others---------------------------------------------------------------
Point& Point::distance(const Point& other)
{
	return *new Point(other._x - _x, other._y - _y);
}
