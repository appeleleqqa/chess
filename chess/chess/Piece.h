#pragma once

#include "Square.h"

class Piece : public Square
{
public:
	Piece();
	Piece(bool white, Point point);
	virtual bool isValidMove(Point point, bool test) = 0;
	virtual char returnCharacter() = 0;
	bool isWhite();
protected:
	bool _isWhite;
	char _character;
};