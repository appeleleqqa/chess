#include "Knight.h"

//constructor
Knight::Knight(bool isWhite, Point point) : Piece(isWhite, point)
{
	if (isWhite)
	{
		_character = 'N';
	}
	else
	{
		_character = 'n';
	}
}

//checks if the move is valid for this sertain piece
bool Knight::isValidMove(Point point, bool test)
{
	Point dis = _point.distance(point);
	//the move has to only include 1 and 2 without doubles
	if (abs(dis.getX()) == 1)
	{
		if (abs(dis.getY()) == 2)
			return true;
	}
	if (abs(dis.getX()) == 2)
	{
		if (abs(dis.getY()) == 1)
			return true;
	}
	return false;
}

//returns the character that represents this piece
char Knight::returnCharacter()
{
	return _character;
}