#include "Blank.h"

//constructor
Blank::Blank(Point point) : Square(point) {}

//returns the charecter that represents this unit
char Blank::returnCharacter()
{
	return '#';
}