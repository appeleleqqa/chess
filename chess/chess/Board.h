#pragma once

#include <iostream>
#include <string>
#include "Point.h"
#include "Square.h"
#include "Blank.h"
#include "Pawn.h"
#include "Rook.h"
#include "Knight.h"
#include "Bishop.h"
#include "Queen.h"
#include "King.h"
class Pawn;

class Board 
{
public:
	Board(bool isWhite);
	std::string getBoardStr();
	void move(Point src, Point dst);
	Square *getSquare(Point point);
	void putSquare(Square* square, Point point);
	bool somethingIntheWay(Point src, Point dst);
	Piece* whatIsInTheWay(Point src, Point dst);
	bool isInDanger(Point src, bool isWhite);
	void print();
	static Point* stringToPoint(std::string);

	int makeATurn(bool turn, std::string input);
	void swichPawn(bool turn);
	void printCode(int moveCode);
private:
	Square* _board[8][8];
	bool _isWhite;
	static King* whiteKing;
	static King* blackKing;
};