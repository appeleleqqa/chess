#pragma once
#include "Point.h"

class Square
{
public:
	Square();
	Square(Point point);
	void setPoint(Point point);
	Point getPoint();
	virtual char returnCharacter() = 0;
protected:
	Point _point;
};