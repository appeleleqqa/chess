#include "King.h"

//constructor
King::King(bool isWhite, Point point) : Piece(isWhite, point)
{
	_point = point;
	_isWhite = isWhite;
	if (isWhite)
	{
		_character = 'K';
	}
	else
	{
		_character = 'k';
	}
}

//checks if this move is valid for this piece
bool King::isValidMove(Point point, bool test)
{
	Point dis = _point.distance(point);
	if (dis.getX() != 1 && dis.getX() != 0 && dis.getX() != -1)
		return false;
	if (dis.getY() != 1 && dis.getY() != 0 && dis.getY() != -1)
		return false;
	return true;
}

//returns a character that represents this unit
char King::returnCharacter()
{
	return _character;
}