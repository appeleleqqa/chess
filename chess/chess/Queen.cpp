#include "Queen.h"

//constructor
Queen::Queen(bool isWhite, Point point) : Piece(isWhite, point)
{
	if (isWhite)
	{
		_character = 'Q';
	}
	else
	{
		_character = 'q';
	}
}

//checks if the move is valid for this piece
bool Queen::isValidMove(Point point, bool test)
{
	Point dis = _point.distance(point);
	//checks if its going in a diagonal line
	if (abs(dis.getX()) == abs(dis.getY()))
	{
		return true;
	}
	//checks if it goes in a strait line
	if (dis.getX() == 0)
	{
		return true;
	}
	else if (dis.getY() == 0)
	{
		return true; 
	}
	return false;
}

//returns the charater that represents this unit
char Queen::returnCharacter()
{
	return _character;
}