#include "Board.h"

King* Board::whiteKing = nullptr;
King* Board::blackKing = nullptr;

//the constructor fot the board
//isWhite: who is starting true = white
//                         false = black
Board::Board(bool isWhite) : _isWhite(isWhite)
{
	int i = 0, j = 0;
	Pawn::setStart(isWhite);
	Pawn::setBoard(this);
	//an array of chars incase the starting player is white
	char tempWhite[8][8] = { {'r', 'n', 'b', 'q', 'k', 'b', 'n', 'r'},
		{'p', 'p', 'p', 'p', 'p', 'p', 'p', 'p'},
		{'#', '#', '#', '#', '#', '#', '#', '#'},
		{'#', '#', '#', '#', '#', '#', '#', '#'},
		{'#', '#', '#', '#', '#', '#', '#', '#'},
		{'#', '#', '#', '#', '#', '#', '#', '#'},
		{'P', 'P', 'P', 'P', 'P', 'P', 'P', 'P'},
		{'R', 'N', 'B', 'Q', 'K', 'B', 'N', 'R'} };
	//an array of chars incase the starting player is black
	char tempBlack[8][8] = { {'R', 'N', 'B', 'K', 'Q', 'B', 'N', 'R'},
		{'P', 'P', 'P', 'P', 'P', 'P', 'P', 'P'},
		{'#', '#', '#', '#', '#', '#', '#', '#'},
		{'#', '#', '#', '#', '#', '#', '#', '#'},
		{'#', '#', '#', '#', '#', '#', '#', '#'},
		{'#', '#', '#', '#', '#', '#', '#', '#'},
		{'p', 'p', 'p', 'p', 'p', 'p', 'p', 'p'},
		{'r', 'n', 'b', 'k', 'q', 'b', 'n', 'r'} };

	//a loop that goes over both dimentions of the board
	for (i = 0; i < 8; i++)
	{
		for (j = 0; j < 8; j++)
		{
			//chooses the coresponding char for the board to be correct acording to the starting player
			switch (isWhite ? tempWhite[i][j] : tempBlack[i][j])
			{
			//----------------------------------------------------black pieces--------------------------------------
			case 'p':
				_board[i][j] = new Pawn(false, Point(i, j));
				break;
			case 'r':
				_board[i][j] = new Rook(false, Point(i, j));
				break;
			case 'n':
				_board[i][j] = new Knight(false, Point(i, j));
				break;
			case 'b':
				_board[i][j] = new Bishop(false, Point(i, j));
				break;
			case 'k':
				_board[i][j] = new King(false, Point(i, j));
				blackKing = (King*)_board[i][j];
				break;
			case 'q':
				_board[i][j] = new Queen(false, Point(i, j));
				break;
			//----------------------------------------------------white pieces---------------------------------------
			case 'P':
				_board[i][j] = new Pawn(true, Point(i, j));
				break;
			case 'R':
				_board[i][j] = new Rook(true, Point(i, j));
				break;
			case 'N':
				_board[i][j] = new Knight(true, Point(i, j));
				break;
			case 'B':

				_board[i][j] = new Bishop(true, Point(i, j));
				break;
			case 'K':
				_board[i][j] = new King(true, Point(i, j));
				whiteKing = (King*)_board[i][j];
				break;
			case 'Q':
				_board[i][j] = new Queen(true, Point(i, j));
				break;
			//black
			case '#':
				_board[i][j] = new Blank(Point(i, j));
				break;
			}
		}
	}
	
}

//converts the board's current state to a 66 long string that can be sent to the interfacce
std::string Board::getBoardStr()
{
	std::string boardStr;
	int i = 0, j = 0;

	for (i = 0; i < 8; i++)
	{
		for (j = 0; j < 8; j++)
		{
			boardStr += _board[i][j]->returnCharacter();
		}
	}
	boardStr += _isWhite ? '0' : '1';

	return boardStr;
}

//moves a unit in one piece to the other
void Board::move(Point src, Point dst)
{
	getSquare(src)->setPoint(dst);
	_board[dst.getX()][dst.getY()] = getSquare(src);
	_board[src.getX()][src.getY()] = new Blank(src);
}

//gets the square that is located at a certain point in the board
Square* Board::getSquare(Point point)
{
	return _board[point.getX()][point.getY()];
}

void Board::putSquare(Square* square, Point point)
{
	_board[point.getX()][point.getY()] = square;
}

//checks if some thing is in the way of two linear points
bool Board::somethingIntheWay(Point src, Point dst)
{
	int x, y;
	//NOTE: x is vertical(up/down) and y is horizontal(left/right)
	Point dis = src.distance(dst);
	if (dis.getX() != 0)//division by 0
	{
		x = abs(dis.getX()) / dis.getX();//checks if x or y are possitive or not
	}
	else
	{
		x = 0;
	}
	//same for y
	if (dis.getY() != 0)
	{
		y = abs(dis.getY()) / dis.getY();
	}
	else
	{
		y = 0;
	}
	//if its going right/left(y = 1 = right, y = -1 = left) 
	if (dis.getX() == 0)
	{
		//for every square in the way
		for (int i = 1; i < abs(dis.getY()); i++)
		{
			//check if its not black
			if (getSquare(Point(src.getX(), src.getY() + i * y))->returnCharacter() != '#')//i*y will go up or down accroding to the direction in which you are trying to go
				return true;
		}
	}
	//same for up or down(up = -1, down = 1)
	else if (dis.getY() == 0)
	{
		for (int i = 1; i < abs(dis.getX()); i++)
		{
			if (getSquare(Point(src.getX() + i * x, src.getY()))->returnCharacter() != '#')//i*x will go left or right accroding to the direction in which you are trying to go
				return true;
		}
	}
	//if we are going in a line that is going to one of sides(decided by x's and y's positivity)
	else if (abs(dis.getY()) == abs(dis.getX()))
	{
		for (int i = 1; i < abs(dis.getX()); i++)
		{
			if (getSquare(Point(src.getX() + i * x, src.getY() + i * y))->returnCharacter() != '#')
				return true;
		}
	}
	return false;
}

//prints the content of the board
void Board::print()
{
	for  (int i = 0; i < 8; i++)
	{
		std::cout << 8 - i << "| ";
		for (int j = 0; j < 8; j++)
		{
			std::cout << _board[i][j]->returnCharacter() << " ";
		}
		std::cout << std::endl;
	}
	std::cout << "   _______________ " << std::endl << "   a b c d e f g h" << std::endl;
}

//converts a 2 character long string to a point that fits the board array
Point* Board::stringToPoint(std::string st)
{
	//checks if the string is valid
	if (st.length() != 2 || st[0] > 'h' || st[0] < 'a' || st[1] < '0' || st[1] >'8')
		return nullptr;
	int x = '8' - st[1]; //'1' in the array will be 7 and '8' will be 0
	int y = st[0] - 'a';//'a' will be 0 and 'h' will be 7
	return new Point(x, y);
}

//checks for a piece between two points
Piece* Board::whatIsInTheWay(Point src, Point dst)
{
	int x, y;
	//NOTE: x is vertical(up/down) and y is horizontal(left/right)
	Point dis = src.distance(dst);
	if (dis.getX() != 0)//division by 0
	{
		x = abs(dis.getX()) / dis.getX();//checks if x or y are possitive or not
	}
	else
	{
		x = 0;
	}
	//same for y
	if (dis.getY() != 0)
	{
		y = abs(dis.getY()) / dis.getY();
	}
	else
	{
		y = 0;
	}
	//if its going right/left(y = 1 = right, y = -1 = left) 
	if (dis.getX() == 0)
	{
		//for every square in the way
		for (int i = 1; i < abs(dis.getY()) + 1; i++)
		{
			//check if its not black
			if (getSquare(Point(src.getX(), src.getY() + i * y))->returnCharacter() != '#')//i*y will go up or down accroding to the direction in which you are trying to go
				return (Piece*)getSquare(Point(src.getX(), src.getY() + i * y));
		}
	}
	//same for up or down(up = -1, down = 1)
	else if (dis.getY() == 0)
	{
		for (int i = 1; i < abs(dis.getX()) + 1; i++)
		{
			if (getSquare(Point(src.getX() + i * x, src.getY()))->returnCharacter() != '#')//i*x will go left or right accroding to the direction in which you are trying to go
				return (Piece*)getSquare(Point(src.getX() + i * x, src.getY()));
		}
	}
	//if we are going in a line that is going to one of sides(decided by x's and y's positivity)
	else if (abs(dis.getY()) == abs(dis.getX()))
	{
		for (int i = 1; i < abs(dis.getX()) + 1; i++)
		{
			if (getSquare(Point(src.getX() + i * x, src.getY() + i * y))->returnCharacter() != '#')
				return (Piece*)getSquare(Point(src.getX() + i * x, src.getY() + i * y));
		}
	}
	return nullptr;
}

//checks if a sertain spot is in danger from the enemy
bool Board::isInDanger(Point src, bool isWhite)
{
	int x = 0, y = 0;
	Square* sq;
	//checks the line up
	Piece* threat = whatIsInTheWay(src, Point(0, src.getY()));
	if (threat != nullptr)
	{
		if (threat->isWhite() != isWhite && threat->isValidMove(src, true))
		{
			return true;
		}
	}
	//checks the line down
	threat = whatIsInTheWay(src, Point(7, src.getY()));
	if (threat != nullptr)
	{
		if (threat->isWhite() != isWhite && threat->isValidMove(src, true))
		{
			return true;
		}
	}
	//checks the line left
	threat = whatIsInTheWay(src, Point(src.getX(), 0));
	if (threat != nullptr)
	{
		if (threat->isWhite() != isWhite && threat->isValidMove(src, true))
		{
			return true;
		}
	}
	//checks the line right
	threat = whatIsInTheWay(src, Point(src.getX(), 7));
	if (threat != nullptr)
	{
		if (threat->isWhite() != isWhite && threat->isValidMove(src, true))
		{
			return true;
		}
	}
	//checks the diagonal line that is going up and right
	for (x = src.getX(), y = src.getY(); x > 0 && y < 7; x--, y++);
	threat = whatIsInTheWay(src, Point(x, y));
	if (threat != nullptr)
	{
		if (threat->isWhite() != isWhite && threat->isValidMove(src, true))
		{
			return true;
		}
	}
	//checks the diagonal line that is going down and right
	for (x = src.getX(), y = src.getY(); x < 7 && y < 7; x++, y++);
	threat = whatIsInTheWay(src, Point(x, y));
	if (threat != nullptr)
	{
		if (threat->isWhite() != isWhite && threat->isValidMove(src, true))
		{
			return true;
		}
	}
	//checks the diagonal line that is going up and left
	for (x = src.getX(), y = src.getY(); x > 0 && y >0; x--, y--);
	threat = whatIsInTheWay(src, Point(x, y));
	if (threat != nullptr)
	{
		if (threat->isWhite() != isWhite && threat->isValidMove(src, true))
		{
			return true;
		}
	}
	//checks the diagonal line that is going down and left
	for (x = src.getX(), y = src.getY(); x < 7 && y > 0; x++, y--);
	threat = whatIsInTheWay(src, Point(x, y));
	if (threat != nullptr)
	{
		if (threat->isWhite() != isWhite && threat->isValidMove(src, true))
		{
			return true;
		}
	}
	//checks the 8 possiblities of knights that would be able to get to our place
	x = src.getX() + 1;
	y = src.getY() + 2;
	if (!(x > 7 || x < 0 || y > 7 || y < 0))
	{
		sq = getSquare(Point(x, y));
		if (sq->returnCharacter() != '#')
		{
			threat = (Piece*)sq;
			if (threat->isWhite() != isWhite && threat->isValidMove(src, true))
			{
				return true;
			}
		}
	}
	x = src.getX() - 1;
	y = src.getY() + 2;
	if (!(x > 7 || x < 0 || y > 7 || y < 0))
	{
		sq = getSquare(Point(x, y));
		if (sq->returnCharacter() != '#')
		{
			threat = (Piece*)sq;
			if (threat->isWhite() != isWhite && threat->isValidMove(src, true))
			{
				return true;
			}
		}
	}
	x = src.getX() + 1;
	y = src.getY() - 2;
	if (!(x > 7 || x < 0 || y > 7 || y < 0))
	{
		sq = getSquare(Point(x, y));
		if (sq->returnCharacter() != '#')
		{
			threat = (Piece*)sq;
			if (threat->isWhite() != isWhite && threat->isValidMove(src, true))
			{
				return true;
			}
		}
	}
	x = src.getX() - 1;
	y = src.getY() - 2;
	if (!(x > 7 || x < 0 || y > 7 || y < 0))
	{
		sq = getSquare(Point(x, y));
		if (sq->returnCharacter() != '#')
		{
			threat = (Piece*)sq;
			if (threat->isWhite() != isWhite && threat->isValidMove(src, true))
			{
				return true;
			}
		}
	}
	x = src.getX() + 2;
	y = src.getY() + 1;
	if (!(x > 7 || x < 0 || y > 7 || y < 0))
	{
		sq = getSquare(Point(x, y));
		if (sq->returnCharacter() != '#')
		{
			threat = (Piece*)sq;
			if (threat->isWhite() != isWhite && threat->isValidMove(src, true))
			{
				return true;
			}
		}
	}
	x = src.getX() - 2;
	y = src.getY() + 1;
	if (!(x > 7 || x < 0 || y > 7 || y < 0))
	{
		sq = getSquare(Point(x, y));
		if (sq->returnCharacter() != '#')
		{
			threat = (Piece*)sq;
			if (threat->isWhite() != isWhite && threat->isValidMove(src, true))
			{
				return true;
			}
		}
	}
	x = src.getX() + 2;
	y = src.getY() - 1;
	if (!(x > 7 || x < 0 || y > 7 || y < 0))
	{
		sq = getSquare(Point(x, y));
		if (sq->returnCharacter() != '#')
		{
			threat = (Piece*)sq;
			if (threat->isWhite() != isWhite && threat->isValidMove(src, true))
			{
				return true;
			}
		}
	}
	x = src.getX() - 2;
	y = src.getY() - 1;
	if (!(x > 7 || x < 0 || y > 7 || y < 0))
	{
		sq = getSquare(Point(x, y));
		if (sq->returnCharacter() != '#')
		{
			threat = (Piece*)sq;
			if (threat->isWhite() != isWhite && threat->isValidMove(src, true))
			{
				return true;
			}
		}
	}
	return false;
}

/*
method to make a single chess game turn, updating the board and returning appropriate code
input: boolian current turn (true = white, false = black), move's string representative
output: user's move outcome code
list of codes:
0 - valid move
1 - valid move, made a check
2 - invalid move, source square has no current player's piece
3 - invalid move, in destination square there is a piece of current player
4 - invalid move, this move will cause check on current player
*5 - invalid move, invalid square index(s)
6 - invalid move, piece unavailable to perfore this move
7 - invalid move, source square and destination square are the same\
8 - valid move, made a checkmate!
*/
int Board::makeATurn(bool turn, std::string input)
{
	int isValid;
	char choice;
	Point* src = nullptr, * dst = nullptr;
	Square* tempSquare = nullptr;

	if (input.length() == 4)
	{
		src = Board::stringToPoint(input.substr(0, 2));
		dst = Board::stringToPoint(input.substr(2, 2));
	}
	//if the input length isn't valid
	else
	{
		return 5;
	}
	//if the input is invalid
	if (src == nullptr || dst == nullptr)
	{
		return 5;
	}

	//if the source square and the destination one are the same
	if (dst->getX() == src->getX() && dst->getY() == src->getY())
	{
		return 7;
	}
	//if the source square is empty
	if (getSquare(*src)->returnCharacter() == '#')
	{
		return 2;
	}
	Piece* source = (Piece*)getSquare(*src);//since we know this isn't blank it has to be a piece
	//if the source square contains a piece that belongs to the other player
	if (source->isWhite() != turn)
	{
		return 2;
	}
	//if the destination square has a chance to belong to the same player
	if (getSquare(*dst)->returnCharacter() != '#')
	{
		Piece* dest = (Piece*)getSquare(*dst);//since we know this isn't blank it has to be a piece
		//if it does
		if (dest->isWhite() == turn)
		{
			return 3;
		}
	}
	//if we got this far we only need to check if the piece can atually preform the move
	if (!source->isValidMove(*dst, false))
	{
			return 6;
	}
	//if something is in the way
	if (source->returnCharacter() != 'n' && source->returnCharacter() != 'N')
	{
			if (somethingIntheWay(*src, *dst))
			{
				return 6;
			}
	}
	
	//check if move will cause check on current player
	tempSquare = getSquare(*dst);
	move(*src, *dst);
	if (isInDanger(turn ? whiteKing->getPoint() : blackKing->getPoint(), turn))
	{
		move(*dst, *src);
		putSquare(tempSquare, *dst);
		return 4;
	}

	//check if check was made on opponent
	if (source->isValidMove(turn ? blackKing->getPoint() : whiteKing->getPoint(),true))
	{
		if (turn ? source->returnCharacter() == 'N' : source->returnCharacter() == 'n')
			return 1;
		if (!somethingIntheWay(*dst, turn ? blackKing->getPoint() : whiteKing->getPoint()))
			return 1;
	}
		
	//bonus

	swichPawn(turn);

	return 0;
}

//checks first and last line for a pawn, if found asks user for which piece to replace the pawn with and updated board
void Board::swichPawn(bool turn)
{
	int i;
	char letter;
	
	for (i = 0; i < 8; i++)
	{
		if (_board[0][i]->returnCharacter() == 'p' || _board[0][i]->returnCharacter() == 'P')
		{
			std::cout << "one of your pawns got to the last line!\nwhat whould you like to swich it to?(q, n, b, r only): ";
			std::cin >> letter;

			switch (letter)
			{
			case 'q':
				_board[0][i] = new Queen(_isWhite, Point(0, i));
				break;
			case 'n':
				_board[0][i] = new Knight(_isWhite, Point(0, i));
				break;
			case 'b':
				_board[0][i] = new Bishop(_isWhite, Point(0, i));
				break;
			case 'r':
				_board[0][i] = new Rook(_isWhite, Point(0, i));
				break;
			default:
				std::cout << "unvalid letter, changing to a queen\n";
				_board[0][i] = new Queen(_isWhite, Point(0, i));
				break;
			}
		}
		else if (_board[7][i]->returnCharacter() == 'p' || _board[7][i]->returnCharacter() == 'P')
		{
			std::cout << "one of your pawns got to the last line!\nwhat whould you like to swich it to?(q, n, b, r only): ";
			std::cin >> letter;

			switch (letter)
			{
			case 'q':
				_board[0][i] = new Queen(_isWhite, Point(0, i));
				break;
			case 'n':
				_board[0][i] = new Knight(_isWhite, Point(0, i));
				break;
			case 'b':
				_board[0][i] = new Bishop(_isWhite, Point(0, i));
				break;
			case 'r':
				_board[0][i] = new Rook(_isWhite, Point(0, i));
				break;
			default:
				std::cout << "unvalid letter, changing to a queen\n";
				_board[0][i] = new Queen(_isWhite, Point(0, i));
				break;
			}
		}
	}
}

/*
method to print appropriate messege given the move's code
input: int move's code
*/
void Board::printCode(int moveCode)
{
	switch (moveCode)
	{
	case 0:
		std::cout << "Valid move!" << std::endl;
		break;
	case 1:
		std::cout << "Valid move, you made a chess!" << std::endl;
		break;
	case 2:
		std::cout << "Invalid move, that square doesn't belong to you!" << std::endl;
		break;
	case 3:
		std::cout << "Invalid move, the destination square contains a unit of your own!" << std::endl;
		break;
	case 4:
		std::cout << "Invalid move, this move will result in a chess on you!" << std::endl;
		break;
	case 5:
		std::cout << "Invalid move, invalid index." << std::endl;
		break;
	case 6:
		std::cout << "Invalid move, this move isn't valid for this peice!" << std::endl;
		break;
	case 7:
		std::cout << "Invalid move, both destenation and source indexes are the same!" << std::endl;
		break;
	case 8:
		std::cout << "Valid move, you made a checkmate" << std::endl;
		break;
	default:
		break;
	}
}