#pragma once

#include "Piece.h"
#include <cmath>

class Bishop : public Piece
{
public:
	Bishop(bool isWhite, Point point);
	bool isValidMove(Point point, bool test);
	char returnCharacter();
};