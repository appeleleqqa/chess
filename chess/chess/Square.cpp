#include "Square.h"

//default constructor
Square::Square()
{
	_point = Point();
}

//the constructor that you are atually supposed to use
Square::Square(Point point)
{
	_point = point;
}

//setter for the place of the square
void Square::setPoint(Point point)
{
	_point = point;
}

//getter for the point of the square
Point Square::getPoint()
{
	return _point;
}