#pragma once

#include "Piece.h"
#include "Board.h"
class Board;

class Pawn : public Piece
{
private:
	bool _firstMove;
	static bool _isWhiteStart;
	static Board* _board;
public:
	Pawn(bool isWhite, Point point);
	bool isValidMove(Point point, bool test);
	char returnCharacter();
	static void setStart(bool isWhite);
	static void setBoard(Board* board);
};