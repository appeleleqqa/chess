#pragma once
#include "Square.h"

class Blank : public Square
{
public:
	Blank(Point point);
	char returnCharacter();
};