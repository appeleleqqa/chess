#pragma once

#include "Piece.h"

class King : public Piece
{
public:
	King(bool isWhite, Point point);
	bool isValidMove(Point point, bool test);
	char returnCharacter();
};