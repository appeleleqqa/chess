#pragma once

#include "Piece.h"

class Rook : public Piece
{
public:
	Rook(bool isWhite, Point point);
	bool isValidMove(Point point, bool test);
	char returnCharacter();
};