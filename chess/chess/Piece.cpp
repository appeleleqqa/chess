#include "Piece.h"
//-----------------------------------------------------------constructors------------------------------------------------------------
Piece::Piece()
{
	_isWhite = false;
	_character = 'a';
}

Piece::Piece(bool white, Point point) : Square(point)
{
	_isWhite = white;
	_character = 'a';
}
//--------------------------------------------------------------getters-------------------------------------------------------------
bool Piece::isWhite()
{
	return _isWhite;
}