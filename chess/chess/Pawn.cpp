#include "Pawn.h"

bool Pawn::_isWhiteStart = true;
Board* Pawn::_board;

//constructor
Pawn::Pawn(bool isWhite, Point point) : Piece(isWhite, point)
{
	_firstMove = true;
	if (isWhite)
	{
		_character = 'P';
	}
	else
	{
		_character = 'p';
	}
}

//checks if the move is valid for this spesific piece
bool Pawn::isValidMove(Point point, bool test)
{
	Point dis = _point.distance(point);
	int positivity = _isWhiteStart ? -1 : 1;

	
			
	if (dis.getX() == 0)
		return false;
	if((dis.getX()/abs(dis.getX()) ==  positivity) != _isWhite)
		return false;
	if (_firstMove)
	{
		if (!((_board->getSquare(point)->returnCharacter() != '#' && abs(dis.getY()) == 1 && abs(dis.getX()) == 1) || (_board->getSquare(point)->returnCharacter() == '#' && dis.getY() == 0 && abs(dis.getX()) <= 2)))
			return false;
		if(!test)
			_firstMove = false;
	}
	else
	{
		if (!((_board->getSquare(point)->returnCharacter() != '#' && abs(dis.getY()) == 1 && abs(dis.getX()) == 1) || (_board->getSquare(point)->returnCharacter() == '#' && dis.getY() == 0 && abs(dis.getX()) == 1)))
			return false;
	}
	return true;
}

//returns the character that represnts this unit
char Pawn::returnCharacter()
{
	return _character;
}

//sets who the starting player is for the pawn
void Pawn::setStart(bool isWhite)
{
	Pawn::_isWhiteStart = isWhite;
}


void Pawn::setBoard(Board* board)
{
	Pawn::_board = board;
}