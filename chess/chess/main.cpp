#include "Board.h"
#include "Pipe.h"
#include <iostream>
#include <thread>

using std::cout;
using std::endl;
using std::string;

bool connect(Pipe& p);

int main(void)
{
	Pipe p;
	char msgToGraphics[1024];
	string msgFromGraphics = "";

	char temp;
	bool turn;
	bool console;
	int moveCode = 0;
	string input;

	cout << "press 'c' to play on console or other for frontend interface: ";
	cin >> temp;
	console = (temp == 'c'); //true = console, false = frontend interface

	if (!console)
	{
		if (!connect(p))
			return 0;
	}
	cout << "who is starting('b' for black other for white): ";
	cin >> temp;
	turn = (temp != 'b'); //true = white, false = black
	Board board(turn);

	if (!console)
	{
		strcpy_s(msgToGraphics, board.getBoardStr().c_str());

		p.sendMessageToGraphics(msgToGraphics);   // send the board string

		msgFromGraphics = p.getMessageFromGraphics(); // get message from graphics
	}
	while (msgFromGraphics != "quit" && moveCode != 8)// TODO: add checkmate
	{
		if (!console)
		{
			moveCode = board.makeATurn(turn, msgFromGraphics);
			strcpy_s(msgToGraphics, to_string(moveCode).c_str());

			p.sendMessageToGraphics(msgToGraphics); // return result to graphics

			msgFromGraphics = p.getMessageFromGraphics(); // get message from graphics
		}
		else
		{
			board.print();
			cout << "it's " << (turn ? "white's" : "black's") << " turn. enter move: ";
			cin >> input;
			board.printCode(moveCode = board.makeATurn(turn, input));
		}
			
		if(moveCode == 0 || moveCode == 1)
			turn = !turn;
	}

	p.close();
	return 0;
}

//function to connect to frontend
bool connect(Pipe& p)
{
	srand(time_t(NULL));
	bool isConnect = p.connect();

	string ans;
	while (!isConnect)
	{
		cout << "cant connect to graphics" << endl;
		cout << "Do you try to connect again or exit? (0-try again, 1-exit)" << endl;
		std::cin >> ans;

		if (ans == "0")
		{
			cout << "trying connect again.." << endl;
			Sleep(5000);
			isConnect = p.connect();
		}
		else
		{
			p.close();
			return false;
		}
	}
	return true;
}