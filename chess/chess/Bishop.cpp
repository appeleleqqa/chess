#include "Bishop.h"

//constructor
Bishop::Bishop(bool isWhite, Point point) : Piece(isWhite, point)
{
	_isWhite = isWhite;
	if (isWhite)
	{
		_character = 'B';
	}
	else
	{
		_character = 'b';
	}
}

//checks if the move is valid for this unit
bool Bishop::isValidMove(Point point, bool test)
{
	Point dis = _point.distance(point);
	if (abs(dis.getX()) == abs(dis.getY()))
	{
		return true;
	}
	return false;
}

//returns the character that represents this piece
char Bishop::returnCharacter()
{
	return _character;
}