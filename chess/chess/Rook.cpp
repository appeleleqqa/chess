#include "Rook.h"

//constructor
Rook::Rook(bool isWhite, Point point) : Piece(isWhite, point)
{
	if (isWhite)
	{
		_character = 'R';
	}
	else
	{
		_character = 'r';
	}
}

//checks if the move is valid for this piece
bool Rook::isValidMove(Point point, bool test)
{
	Point dis = _point.distance(point);
	if (dis.getX() == 0)
	{
		return true;
	}
	else if (dis.getY() == 0)
	{
		return true;
	}
	return false;
}

//returns a character that represents this unit
char Rook::returnCharacter()
{
	return _character;
}