#pragma once

#include "Piece.h"

class Queen : public Piece
{
public:
	Queen(bool isWhite, Point point);
	bool isValidMove(Point point, bool test);
	char returnCharacter();
};