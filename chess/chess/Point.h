#pragma once

#include <cmath>

class Point
{
public:
	Point();
	Point(int x, int y);
	Point(const Point& other);
	int getX();
	int getY();
	Point& operator=(const Point& other);
	Point& distance(const Point& other);

private:
	int _x;
	int _y;
};